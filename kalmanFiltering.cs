using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using LightweightMatrixCSharp;
using System;


public class EKalmanF : MonoBehaviour
{

    public enum DynSysModel
    {
        RBDynTrans,
        RBDynTransRot
    }

    public OVRCameraRig VirtualCamera;

    private Dynamics Dyn;

    // Dynamical System Model
    DynSysModel DynSysModel_type = DynSysModel.RBDynTrans;
    bool _DynSysModel = false;

    private int NumberOfInputs;
    private int ModelDim;
    private int Observability;

    private bool StateObservability;

    // Dynamic constraints: predictions in the absence of correct tracking data
    int NumberOfConsecutivePredictions;
    int MaxNumberOfPredictions;
    double MaxL2InterDist;

    /// <summary>
    ///  Random Walk Model
    ///  Gyroscope/accelerometer/magnetometer sensors fusion
    ///  private double[] tau = new double[3];
    /// </summary>


    // Translational model         
    private Matrix X;
    private Matrix Xp;

    // Time increment
    private double dt;

    // Process noise model: standard deviation σx
    double SX;

    // Measurement noise model: standard deviation σz
    double SZ;

    // Input-system dynamics
    // <x,y,z, dx, dy, dz, dxx, dyy, dzz>
    double[,] x;
    double[,] dx;
    double[,] ddx;


    /// State transition model            
    private Matrix F;
    private Matrix Ft;
    private Matrix I;
    private Matrix D;

    // Control-input model
    private Matrix B;

    // Control-input
    private Matrix u;
    private Matrix _u;

    // Observation model which maps the true state space into the observable space
    private Matrix C;
    private Matrix Ct;

    // Sx: Process covariance matrix        
    private Matrix Ex;

    // H Hessian matrix        
    private Matrix H;

    // Jacobian matrix
    private Matrix J;

    // P: Predicted process covariance matrix
    private Matrix P;

    // Ez: Measurement covariance matrix
    private Matrix Ez;

    // Observable states
    Matrix Z;

    // Kalman Gain
    Matrix K;


    void Awake()
    {

        // Positions, velocities, and accelerations
        x = new double[3, 3];
        dx = new double[3, 3];
        ddx = new double[3, 3];
    }

    /// <summary>
    /// FDM: central difference method
    /// </summary>
    public void ComputeDynamics()
    {
        // Positions
        // (n)
        x[2, 0] = X[0, 0]; x[2, 1] = X[1, 0]; x[2, 2] = X[2, 0];

        // (n-1)
        x[1, 0] = x[2, 0]; x[1, 1] = x[2, 1]; x[1, 2] = x[2, 2];

        // (n-2)
        x[0, 0] = x[1, 0]; x[0, 1] = x[1, 1]; x[0, 2] = x[1, 2];

        // Velocities
        double _dt = 2 * dt;
        dx[1, 0] = (x[2, 0] - x[0, 0]) / _dt;
        dx[1, 1] = (x[2, 1] - x[0, 1]) / _dt;
        dx[1, 2] = (x[2, 2] - x[0, 2]) / _dt;


        // Translational motion model
        // Accelerations
        // CDF
        _u[0, 0] = ddx[1, 0] = (dx[2, 0] - dx[0, 0]) / _dt;
        _u[1, 0] = ddx[1, 1] = (dx[2, 1] - dx[0, 1]) / _dt;
        _u[2, 0] = ddx[1, 2] = (dx[2, 2] - dx[0, 2]) / _dt;
    }

    public void SetPhysicalContraints(double InterL2State, int NumberOfPredictions)
    {
        MaxNumberOfPredictions = NumberOfPredictions;
        MaxL2InterDist = InterL2State;
    }

    public void SetDynSysModel(DynSysModel _type)
    {
        DynSysModel_type = _type;

        switch (DynSysModel_type)
        {
            // System states <X>: < x, y, z, xd, yd, zd >
            case DynSysModel.RBDynTrans:
                ModelDim = 6;
                Observability = 3;
                NumberOfInputs = 3;

                break;

            // System states <X>: < x, y, z, xd, yd, zd, qx, qy, qz, qw >
            case DynSysModel.RBDynTransRot:
                ModelDim = 4;
                Observability = 4;
                NumberOfInputs = 4;

                // Hessian of the process covariance matrix
                H = Matrix.ZeroMatrix(ModelDim, ModelDim);

                // Jacobian matrix
                J = Matrix.ZeroMatrix(ModelDim, NumberOfInputs);

                break;

            default:
                throw new NotImplementedException
                    ("Unrecognized Dynamical system model.");
        }

        // Systems's states
        X = Matrix.ZeroMatrix(ModelDim, 1);
        Xp = Matrix.ZeroMatrix(ModelDim, 1);

        // State transition model    
        F = Matrix.ZeroMatrix(ModelDim, ModelDim);
        Ft = Matrix.ZeroMatrix(ModelDim, ModelDim);

        // Control-input model
        B = Matrix.ZeroMatrix(ModelDim, NumberOfInputs);

        // Control-input
        u = Matrix.ZeroMatrix(NumberOfInputs, 1);
        _u = Matrix.ZeroMatrix(NumberOfInputs, 1);

        // Sx: Process covariance matrix
        Ex = Matrix.ZeroMatrix(ModelDim, ModelDim);
        //P = Matrix.ZeroMatrix(ModelDim, ModelDim);
        P = Matrix.IdentityMatrix(ModelDim, ModelDim);

        // Ez: Measurement covariance matrix
        Ez = Matrix.ZeroMatrix(Observability, Observability);

        // Observation model which maps the true state space into the observable space            
        C = Matrix.ZeroMatrix(Observability, ModelDim);
        Ct = Matrix.ZeroMatrix(ModelDim, Observability);

        // Observable states
        Z = Matrix.ZeroMatrix(Observability, 1);

        // Kalman Gain
        K = Matrix.ZeroMatrix(ModelDim, Observability);

        // Eye matrix
        I = Matrix.IdentityMatrix(ModelDim, ModelDim);

        _DynSysModel = true;
    }

    public void SetControlInput(params float[] _inp)
    {
        for (int i = 0; i < NumberOfInputs; ++i)
        {
            u[i, 0] = (double)_inp[i];
        }
    }


    /// <summary>
    /// Linearized error-state system estimation
    /// </summary>
    public void ComputeHessianMat()
    {
        // System states <X>: < x, y, z, xd, yd, zd, qx, qy, qz, qw >
        double qx = X[0, 0];
        double qy = X[1, 0];
        double qz = X[2, 0];
        double qw = X[3, 0];
        double c = dt * dt / 4;

        // Symmetric matrix
        H[0, 0] = SX * c * (qw * qw + qz * qz + qy * qy);
        H[0, 1] = H[1, 0] = c * (-qx * qy);
        H[0, 2] = H[2, 0] = c * (-qx * qz);
        H[0, 3] = H[3, 0] = c * (-qx * qw);

        H[1, 1] = SX * c * (qw * qw + qz * qz + qx * qx);
        H[1, 2] = c * (H[2, 1] = -qy * qz);
        H[1, 3] = c * (H[3, 1] = -qy * qw);

        H[2, 2] = c * (qw * qw + qy * qy + qx * qx);
        H[2, 3] = c * (H[3, 2] = -qz * qw);

        H[3, 3] = SX * c * (qz * qz + qy * qy + qx * qx);
    }



    /// <summary>
    /// Control-input model update        
    /// </summary>
    public void UpdateControlInputModel()
    {
        // System states<X>: < qx, qy, qz, qw >
        double qx = Z[0, 0];
        double qy = Z[1, 0];
        double qz = Z[2, 0];
        double qw = Z[3, 0];


        B[0, 0] = qw; B[0, 1] = qz; B[0, 2] = -qy; B[0, 3] = qx;

        B[1, 0] = -qz; B[1, 1] = qw; B[1, 2] = qx; B[1, 3] = qy;

        B[2, 0] = qy; B[2, 1] = -qx; B[2, 2] = qw; B[2, 3] = qz;

        B[3, 0] = -qx; B[3, 1] = -qy; B[3, 2] = -qz; B[3, 3] = qw;

    }
    

    public void UpdateProcessCovMatrix()
    {
        P += H;
    }


    public void SetInitialQStates(Quaternion Xi)
    {
        // Input-matrix model has also to be updated

        Z[0, 0] = X[0, 0] = (double)Xi.x;
        Z[1, 0] = X[1, 0] = (double)Xi.y;
        Z[2, 0] = X[2, 0] = (double)Xi.z;
        Z[3, 0] = X[3, 0] = (double)Xi.w;

    }


    public void SetInitialStates(Vector3 Xi)
    {
        X[0, 0] = (double)Xi.x;
        X[1, 0] = (double)Xi.y;
        X[2, 0] = (double)Xi.z;

        for (int i = Observability; i < ModelDim; ++i)
            X[i, 0] = 0.0;
    }


    public void ApplyMultiStepAheadPrediction(int n, params float[] _inp)
    {
        Xp = X;
        for (int i = 0; i < n; ++i)
            Xp = F * Xp + B * _u;

    }

    public void InitStateSpaceModel(double _SX, double _SZ, double _dt)
    {
        dt = _dt;
        SX = _SX;
        SZ = _SZ;

        NumberOfConsecutivePredictions = 0;

        // Physical assumptions and constraints
        MaxL2InterDist = 0.5;
        MaxNumberOfPredictions = 5;

        StateObservability = true;

        double a = dt * dt;
        double b = (dt * dt * dt) / 2;
        double c = (dt * dt * dt * dt) / 4;

        switch (DynSysModel_type)
        {
            case DynSysModel.RBDynTrans:
                /// State transition model            
                // Positions
                F[0, 0] = 1; F[0, 3] = dt;
                F[1, 1] = 1; F[1, 4] = dt;
                F[2, 2] = 1; F[2, 5] = dt;

                // Velocities
                F[3, 3] = 1;
                F[4, 4] = 1;
                F[5, 5] = 1;


                // Ex: Process covariance matrix
                // <x,y,z>
                Ex[0, 0] = c; Ex[0, 3] = b;
                Ex[1, 1] = c; Ex[1, 4] = b;
                Ex[2, 2] = c; Ex[2, 5] = b;

                // <xd,yd,zd>
                Ex[3, 0] = b; Ex[3, 3] = a;
                Ex[4, 1] = b; Ex[4, 4] = a;
                Ex[5, 2] = b; Ex[5, 5] = a;

                for (int i = 0; i < Ex.rows; ++i)
                    for (int j = 0; j < Ex.cols; ++j)
                        Ex[i, j] *= SX * SX;

                // Ez: Measurement covariance matrix
                for (int i = 0; i < Ez.rows; ++i)
                    Ez[i, i] = SZ * SZ;

                // Observation model 
                for (int i = 0; i < C.rows; ++i)
                    C[i, i] = 1;

                // Input-control model
                B[0, 0] = a / 2;
                B[1, 1] = a / 2;
                B[2, 2] = a / 2;
                B[3, 0] = dt;
                B[4, 1] = dt;
                B[5, 2] = dt;

                break;

            case DynSysModel.RBDynTransRot:
                /// State transition model                              


                /// State transition model            
                // Quaternion components: <qx,qy,qz,qw>
                F[0, 0] = F[1, 1] = F[2, 2] = F[3, 3] = 1;
                
                // Ex: Process covariance matrix
                // <qx,qy,qz,qw>                    

                for (int i = 0; i < Ex.rows; ++i)
                    Ex[i, i] *= SX * SX;

                // Ez: Measurement covariance matrix
                for (int i = 0; i < Ez.rows; ++i)
                    Ez[i, i] = SZ * SZ;

                // Observation model 
                for (int i = 0; i < C.rows; ++i)
                    C[i, i] = 1;

                // Input-control model
                // [B] = [0]                    

                // Jacobian-matrix
                // [J] = [0]

                // Hessian-matrix
                // [H] = [0]


                break;

            default:
                break;
        }

        // Transpose Process Matrix
        Ft = Matrix.Transpose(F);

        // Transpose Observation Matrix
        Ct = Matrix.Transpose(C);
    }


    public void SetObservedQStates(params float[] _ObsS)
    {

        for (int i = 0; i < Observability; ++i)
        {
            Z[i, 0] = (double)_ObsS[i];

        }
    }


    //  Model-type
    public void SetObservedStates(Vector3 _Z)
    {

        Vector3 dummy = new Vector3((float)X[0, 0], (float)X[1, 0], (float)X[2, 0]);

        // Assumptions, and constraints
        if ((Vector3.Magnitude(_Z - dummy) > MaxL2InterDist) && (NumberOfConsecutivePredictions < MaxNumberOfPredictions))
        {
            StateObservability = false;
            NumberOfConsecutivePredictions++;
        }
        else
        {
            Z[0, 0] = _Z.x;
            Z[1, 0] = _Z.y;
            Z[2, 0] = _Z.z;

            NumberOfConsecutivePredictions = 0;
            StateObservability = true;
        }


        // Restaring the model
        if (NumberOfConsecutivePredictions >= MaxNumberOfPredictions)
        {
            Z[0, 0] = _Z.x;
            Z[1, 0] = _Z.y;
            Z[2, 0] = _Z.z;

            SetInitialStates(_Z);
            NumberOfConsecutivePredictions = 0;
            StateObservability = true;
        }
    }


    public void ResetModelStates()
    {
        for (int i = 0; i < ModelDim; ++i)
            X[i, 0] = 0;
    }

    public Matrix GetUpdatedStates()
    {
        return X;
    }

    public Vector3 GetUpdatedPositions()
    {
        return new Vector3((float)X[0, 0], (float)X[1, 0], (float)X[2, 0]);
    }

    public Quaternion GetUpdatedRotations()
    {
        return new Quaternion((float)X[0, 0], (float)X[1, 0], (float)X[2, 0], (float)X[3, 0]);
    }

    public Vector3 GetMultiStepPredictedStates()
    {
        return new Vector3((float)Xp[0, 0], (float)Xp[1, 0], (float)Xp[2, 0]);
    }


    public void PrintStates()
    {
        switch (DynSysModel_type)
        {
            case DynSysModel.RBDynTrans:

                Debug.Log(
                    X[0, 0].ToString() + "\t" + X[1, 0].ToString() + "\t" + X[2, 0].ToString() + "\t" +
                    X[3, 0].ToString() + "\t" + X[4, 0].ToString() + "\t" + X[5, 0].ToString());
                break;

            default:
                break;
        }
    }



    public void ApplyKalmanFilter()
    {
        // Extended Kalman-Filter
        // Input-Matrix model and Hessian should be computed 
        // Partial modification required
        if (DynSysModel_type == DynSysModel.RBDynTransRot)
        {
            UpdateControlInputModel();
            ComputeHessianMat();
            UpdateProcessCovMatrix();
        }

        // State prediction
        // A priori state estimate without input
        X = F * X + B * u;
        

        if (!StateObservability)
        {
            return;
        }

        // Process covariance matrix prediction            
        P = F * P * Ft + Ex;

        // Compute the Kalman Gain            
        Matrix tmp = C * P * Ct + Ez;

        K = P * Ct * tmp.Invert();
        
        // Update / Correct the state estimate
        // A posteriori state estimate
        X = X + K * (Z - C * X);

        // Update process covariance matrix                        
        P = (I - K * C) * P;

        // Used in the multi-step prediction (CDM)
        ComputeDynamics();
    }
}
